package SeleniumWebElementsInteractions;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x1.tests.ConfigurationTest;

import java.util.List;

public class StackoverflowDropdownMultiselectTest extends ConfigurationTest {
    private Logger logger = LoggerFactory.getLogger(
            StackoverflowDropdownMultiselectTest.class);

    @Test public void test(){
        browser.get("https://stackoverflow.com/");
        logger.info("Click Developers Jobs button");
        browser.findElement(By.id("nav-jobs")).click();

        logger.info("Click Filter icon button");
        browser.findElement(By.cssSelector("button.icon-advanced-search")).click();

        logger.info("Click Compensation tab");
        browser.findElement(By.cssSelector("a[data-tab='Compensation']")).click();

        /**
         * Select class is used for interaction with 'select' element
         */
        Select select = new Select(browser.findElement(By.id("c")));
        List<WebElement> listSelectOptions = select.getOptions();
        for(WebElement selectOption: listSelectOptions){
            System.out.println(selectOption.getAttribute("value"));
            System.out.println(selectOption.getText());
        }
    }
}
