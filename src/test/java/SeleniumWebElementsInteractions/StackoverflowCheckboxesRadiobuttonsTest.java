package SeleniumWebElementsInteractions;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x1.tests.ConfigurationTest;
import x1.tests.KreditechSocialTest;

import java.util.List;

/**
 * Created by Andrey Nazarov on 8/14/2017.
 */
public class StackoverflowCheckboxesRadiobuttonsTest extends ConfigurationTest {
    private Logger logger = LoggerFactory.getLogger(KreditechSocialTest.class);

    @Test public void testCheckbox(){
        browser.get("https://stackoverflow.com/");
        logger.info("Click Developers Jobs button");
        browser.findElement(By.id("nav-jobs")).click();

        logger.info("Click Filter icon button");
        browser.findElement(By.cssSelector("button.icon-advanced-search")).click();

        logger.info("Click Perks tab");
        browser.findElement(By.cssSelector("a[data-tab='Perks']")).click();

        logger.info("Verify that all checkboxes are unchecked");
        List<WebElement> checkboxes = browser.findElements(By.xpath("//fieldset[@data-pane='Perks']//input"));
        for(WebElement checkbox: checkboxes){
            Assert.assertFalse(checkbox.isSelected());
            System.out.println(checkbox.getText());
        }

        logger.info("Check 'Offers remote' checkbox");
        WebElement offersCheckbox = browser.findElement(By.xpath("//input[@name='r']"));
        offersCheckbox.click();

        logger.info("Verify that checkbox is checked");
        Assert.assertTrue(offersCheckbox.isSelected());

    }
}
