package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

import static org.hamcrest.core.StringStartsWith.startsWith;

public class ExpectedExceptionRuleTest {
    /**
     * Rule allows to set expected exception and expected message for the test.
     */
    @Rule public ExpectedException expectedException = ExpectedException.none();

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testExpectedExceptionRuleTest() throws IOException {
        System.out.println("@Test testExpectedExceptionRuleTest");
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("happened?");
        expectedException.expectMessage(startsWith("What"));

        throw new RuntimeException("What happened?");
    }

    /**
     * Expected exception as Test parameter
     */
    @Test(expected = NullPointerException.class) public void testExpectedExceptionTestParameterTest() {
        throw new NullPointerException();
    }

}
