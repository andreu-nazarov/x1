package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class ExternalResourceRuleTest {

    /**
     * Rules provides ability to perform actions before '@Before' method
     * and after '@After' method.
     */
    @Rule public ExternalResource externalResource = new ExternalResource() {
        @Override public Statement apply(Statement base,
                Description description) {
            return super.apply(base, description);
        }

        @Override protected void before() throws Throwable {
            super.before();
            System.out.println(
                    "ExternalResource Rule: action before test method");
        }

        @Override protected void after() {
            super.after();
            System.out
                    .println("ExternalResource Rule: action after test method");
        }
    };

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testExternalResourceRule() {
        System.out.println("@Test testExternalResourceRule");
    }

}
