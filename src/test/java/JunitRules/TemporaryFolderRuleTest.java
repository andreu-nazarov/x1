package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class TemporaryFolderRuleTest {
    /**
     * Rule provides ability to create temporary files during test run and then
     * automatically removes it after test succeed/fail
     */
    @Rule public TemporaryFolder folder = new TemporaryFolder();
    private File file;

    @Before public void init() {
        System.out.println("@Before");
        if (file == null)
            System.out.println("File not initialized");
    }

    @After public void end() {
        System.out.println("@After");
        if (file == null)
            System.out.println("File already removed");
    }

    @Test public void testTemporaryFolderRuleTest() throws IOException {
        File file = folder.newFile("test.txt");
        System.out.println("@Test testTemporaryFolderRuleTest");
        System.out.println(file.exists());
    }
}
