package JunitRules;

import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class TestWatcherRuleTest {
    /**
     * Rules provides ability to perform actions
     * before '@Before' method and
     * after '@After' method and
     * on test succeed and
     * ot test fail.
     */

    @Rule public TestWatcher watcher = new TestWatcher() {
        @Override protected void succeeded(Description description) {
            super.succeeded(description);
            System.out.println("Action when test passes");
        }

        @Override protected void failed(Throwable e, Description description) {
            super.failed(e, description);
            System.out.println("Action when test fails");
        }

        @Override protected void starting(Description description) {
            super.starting(description);
            System.out.println("Action when test starts");
        }

        @Override protected void finished(Description description) {
            super.finished(description);
            System.out.println("Action when test finished");
        }
    };

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testTestWatcherRuleTest1() {
        System.out.println("@Test testTestWatcherRuleTest1");
    }

    @Test public void testTestWatcherRuleTest2() {
        System.out.println("@Test testTestWatcherRuleTest2");
        Assert.assertEquals("asd", "s");
    }

}
