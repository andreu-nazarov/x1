package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;

public class TestNameRuleTest {

    /**
     * Rule provides ability to use test method name within the test.
     */
    @Rule public TestName testName = new TestName();

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testTestNameRuleTest() throws IOException {
        System.out.println("@Test testTestNameRuleTest");
        System.out.println(testName.getMethodName());
    }

}
