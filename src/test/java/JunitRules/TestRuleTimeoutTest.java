package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;

public class TestRuleTimeoutTest {
    /**
     * Each test will use '1000' ms as timeout value
     */
    @Rule public TestRule timeout = new Timeout(1000);

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testTestRuleTimeoutTest1() {
        System.out.println("@Test testTestRuleTimeoutTest1");
    }

    @Test public void testTestRuleTimeoutTest2() {
        System.out.println("@Test testTestRuleTimeoutTest2");
    }

    /**
     * Expected timeout as Test parameter
     */
    @Test(timeout = 2000) public void testTimeoutParameterExceptionTest() {
        System.out.println("@Test testTestRuleTimeoutTest2");
    }
}
