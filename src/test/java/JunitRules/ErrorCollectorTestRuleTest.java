package JunitRules;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.io.IOException;

import static org.hamcrest.core.Is.is;

public class ErrorCollectorTestRuleTest {

    /**
     * Rule provides ability to collect all fails of the test.
     * Don't fail the test on first assertion fail.
     */
    @Rule public ErrorCollector errorCollector = new ErrorCollector();

    @Before public void init() {
        System.out.println("@Before");
    }

    @After public void end() {
        System.out.println("@After");
    }

    @Test public void testErrorCollectorTestRuleTest1() throws IOException {
        System.out.println("@Test testErrorCollectorTestRuleTest1: Assertion1");
        errorCollector.checkThat("Should match", "b", is("a"));
        System.out.println("@Test testErrorCollectorTestRuleTest1: Assertion2");
        errorCollector.checkThat("Should match", "b", is("a"));
    }
}
