package JunitAnnotations;

import org.junit.*;
import org.junit.rules.TestName;

/**
 * Created by Andrey on 13.08.2017.
 */
public class AnnotationsTest {
    /**
     * '@Rule' - allows to use different rules for tests
     */
    @Rule public TestName test = new TestName();

    /**
     * '@BeforeClass' - performs action before class
     */
    @BeforeClass public static void beforeClass(){
        System.out.println("BeforeClass");
    }
    /**
     * '@Before' - performs action before each test method
     */
    @Before public void before(){
        System.out.println("Before");
    }
    /**
     * '@BeforeClass' - performs action after class
     */
    @AfterClass public static void afterClass(){
        System.out.println("AfterClass");
    }
    /**
     * '@After' - performs action after each test method
     */
    @After public void after(){
        System.out.println("After");
    }

    /**
     * '@Test' - marks method as Junit test
     */
    @Test public void test1(){
        System.out.println("Test1");
    }

    /**
     * '@Test(expected = ExpectedException.class)' - test with expected Exception parameter
     */
    @Test(expected = NullPointerException.class) public void test2(){
        System.out.println("Test2");
        throw new NullPointerException();
    }

    /**
     * '@Test(timeout = 1000)' - test with expected timeout parameter
     */
    @Test(timeout = 1000) public void test3(){
        System.out.println("Test3");
    }

    /**
     * '@Ignore' - ignores marked test during test tun
     */
    @Ignore
    @Test
    public void test4(){
        System.out.println("test4");
    }


}
