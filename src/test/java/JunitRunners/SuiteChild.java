package JunitRunners;

import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

/**
 * Created by Andrey Nazarov on 8/14/2017.
 */
public class SuiteChild extends Suite {
    public SuiteChild(Class<?> klass, RunnerBuilder builder)
            throws InitializationError {
        super(klass, builder);
    }
}
