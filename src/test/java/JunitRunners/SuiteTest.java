package JunitRunners;

import JunitAnnotations.AnnotationsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import x1.tests.KreditechSocialTest;

/**
 * Invokes custom runner 'Suite' provided as parameter
 */
@RunWith(Suite.class)
/**
 * List of tests that will run under custom runner
 */
@Suite.SuiteClasses({KreditechSocialTest.class, AnnotationsTest.class})
public class SuiteTest {
}
