package SeleniumJQuerySelector;

import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x1.tests.ConfigurationTest;

import java.util.ArrayList;

public class JQuerySelectorTest extends ConfigurationTest {
    private Logger logger = LoggerFactory.getLogger(JQuerySelectorTest.class);

    @Test public void test(){
        browser.get("https://stackoverflow.com/");
        logger.info("Click Developers Jobs button");

        JavascriptExecutor js = (JavascriptExecutor)browser;

        ArrayList<WebElement> elements = (ArrayList<WebElement>) js.executeScript("return jQuery.find('#nav-jobs');");
        elements.get(0).click();

     }
}
