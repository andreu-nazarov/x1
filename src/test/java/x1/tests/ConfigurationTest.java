package x1.tests;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andrey on 07.08.2017.
 */

public class ConfigurationTest {
    private Logger logger = LoggerFactory.getLogger(ConfigurationTest.class);

    public WebDriver browser;
    private static final String BROWSER_CHROME = "chrome";
    private static final String BROWSER_FIREFOX = "firefox";
    private static final String SCREENSHOTS_PATH = "target/screenshots/";

    public ConfigurationTest() {
        initialize();
        setup();
    }

    private void initialize() {
        if (browser == null) {
            createNewDriverInstance();
        }
    }

    private void createNewDriverInstance() {
        String browser = System.getenv("browser");
        if (BROWSER_CHROME.equals(browser)) {
            ChromeDriverManager.getInstance().setup();
            this.browser = new ChromeDriver();
        } else if (BROWSER_FIREFOX.equals(browser)) {
            FirefoxDriverManager.getInstance().setup();
            this.browser = new FirefoxDriver();
        } else {

            ChromeDriverManager.getInstance().setup();
            this.browser = new ChromeDriver();

            //logger.error("Wrong value was passed to 'browser' variable");
            //throw new RuntimeException();

        }
    }

    private void setup() {
        try {
            browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            browser.manage().window().maximize();
        } catch (Exception e) {
            logger.error("Driver setup failed", e);
        }
    }

    public void destroyDriver() {
        browser.quit();
    }

    /**
     * Method takes a screenshot of the page and copies
     * it to screenshot folder set in system property
     */
    public void takeScreenshot(String name) {
        try {
            if (name == null) {
                name = "";
            }
            // take screenshot
            File scrFile = ((TakesScreenshot) browser)
                    .getScreenshotAs(OutputType.FILE);

            // create unique name based on classname and time
            String uniqueName =
                    name + this.getClass().getSimpleName() + new Date()
                            .getTime();

            // move screenshot to specific folder set in system property
            FileUtils.copyFile(scrFile, new File(SCREENSHOTS_PATH + uniqueName + ".png"));

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Watcher rule watches on test fail/succeed,
     * takes screenshot on test fail and destroys the browser,
     * on succeed destroys the browser
     */
    class ScreenshotTestRule extends TestWatcher {
        @Override protected void failed(Throwable e, Description description) {
            super.failed(e, description);
            takeScreenshot("fail_");
            destroyDriver();
        }

        @Override protected void succeeded(Description description) {
            super.succeeded(description);
            destroyDriver();
        }
    }

}
