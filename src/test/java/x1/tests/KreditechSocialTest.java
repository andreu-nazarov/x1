package x1.tests;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x1.pages.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by Andrey on 07.08.2017.
 */
public class KreditechSocialTest extends ConfigurationTest {
    private Logger logger = LoggerFactory.getLogger(KreditechSocialTest.class);

    private GoogleSearchPage googleSearchPage;
    private KreditechLandingPage kreditechLandingPage;
    private WhatWeDoPage whatWeDoPage;
    private WhatWeArePage whatWeArePage;
    private WorkWithUsPage workWithUsPage;
    private CareersPage careersPage;
    private InvestorRelationsPage investorRelationsPage;
    private PressPage pressPage;
    private MagazinePage magazinePage;

    @Rule public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule();
    @Rule public TestName test = new TestName();

    @Before public void init() {
        browser.get(GoogleSearchPage.PAGE_URL);
        googleSearchPage = new GoogleSearchPage(browser);
    }

    @Test public void kreditechSocialTest() {
        logger.info(test.getMethodName() + " STARTS");
        String searchQuery = "kreditech.com";

        logger.info("STEP 1");
        googleSearchPage.searchFor(searchQuery);
        takeScreenshot("step_end");

        logger.info("STEP 2");
        String searchResultTitle = "Kreditech - Financial Freedom for the Underbanked";
        googleSearchPage.clickSearchResultWithText(searchResultTitle);
        kreditechLandingPage = new KreditechLandingPage(browser);
        takeScreenshot("step_end");

        logger.info("STEP 3 - Linkedin");
        kreditechLandingPage.clickLinkedinSocialButton();
        assertTrue("Kreditech Linkedin account opened", kreditechLandingPage
                .isSocialAccountOpened(Page.LINKEDIN_SOCIAL_ID, "linkedin"));
        takeScreenshot("step_end");

        logger.info("STEP 3 - Facebook");
        kreditechLandingPage.clickFacebookSocialButton();
        assertTrue("Kreditech Facebook account opened", kreditechLandingPage
                .isSocialAccountOpened(Page.FACEBOOK_SOCIAL_ID, "facebook"));
        takeScreenshot("step_end");

        logger.info("STEP 3 - Twitter");
        kreditechLandingPage.clickTwitterSocialButton();
        assertTrue("Kreditech Twitter account opened", kreditechLandingPage
                .isSocialAccountOpened(Page.TWITTER_SOCIAL_ID, "twitter"));
        takeScreenshot("step_end");

        logger.info("STEP 4");
        whatWeDoPage = kreditechLandingPage.mainMenu.clickWhatWeDoMenuItem();
        takeScreenshot("step_end");

        whatWeArePage = whatWeDoPage.mainMenu.clickWhatWeAreMenuItem();
        takeScreenshot("step_end");

        workWithUsPage = whatWeArePage.mainMenu.clickWorkWithUsMenuItem();
        takeScreenshot("step_end");

        careersPage = workWithUsPage.mainMenu.clickCareersMenuItem();
        takeScreenshot("step_end");

        investorRelationsPage = careersPage.mainMenu
                .clickInvestorRelationsMenuItem();
        takeScreenshot("step_end");

        pressPage = investorRelationsPage.mainMenu.clickPressMenuItem();
        takeScreenshot("step_end");

        magazinePage = pressPage.mainMenu.clickMagazineMenuItem();
        takeScreenshot("step_end");

        kreditechLandingPage = magazinePage.mainMenu.clickHomeMenuItem();
        takeScreenshot("step_end");
        logger.info(test.getMethodName() + " ENDS");

    }
}
