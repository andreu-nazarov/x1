package JunitParameeterizedTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import x1.tests.ConfigurationTest;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class ParameterizedTest extends ConfigurationTest {
    private static StringBuffer verificationErrors = new
            StringBuffer();
    private String menuItemName;
    private String expectedPageTitle;

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays
                .asList(new Object[][] {
                        { "Questions", "Newest Questions - Stack Overflow" },
                        { "Developer Jobs", "Developer Jobs, Programming Jobs & More  - Stack Overflow1" },
                        { "Tags", "Tags - Stack Overflow" } });
    }

    public ParameterizedTest(String menuItem, String title)
    {
        this.menuItemName = menuItem;
        this.expectedPageTitle = title;
    }

    @Test
    public void parameterizedTest() throws Exception {
        browser.get("https://stackoverflow.com/");

        try {
            WebElement menuItem = browser.findElement(By.linkText(menuItemName));
            menuItem.click();
            Assert.assertEquals(expectedPageTitle, browser.getTitle());
        } catch (Error e) {
            //Capture and append Exceptions/Errors
            verificationErrors.append(e.toString());
            System.err.println("Assertion Fail "+ verificationErrors.
                    toString());
        }
    }

}
