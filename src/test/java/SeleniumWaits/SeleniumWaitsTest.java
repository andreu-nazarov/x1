package SeleniumWaits;

/**
 * Created by Andrey Nazarov on 11/30/2017.
 */
public class SeleniumWaitsTest {
    public static void main(String[] args) {
        /**
         * factorial
         int a = fact(5);
         System.out.println(a);
         */
        /**
         * string
         ArrayList<String> list = new ArrayList<String>();
         list.add("a");
         list.add("b");
         list.add("c");
         String result = "";

         for (int i = 0; i < list.size(); i++) {
         if (i < list.size() - 1) {
         result += list.get(i) + ",";
         } else {
         result += list.get(i);
         }
         }

         System.out.println(result);
         */
        /**
         * array
         int arr[][] = { {1,0,0}, {0,1,0}, {0,0,1} };
         String res = "";
         for(int i = 0; i < arr.length; i++) {
         res+=arr[i][i];
         }

         System.out.println(res);
         res="";

         for(int i = 0; i < arr.length; i++) {
         res+= arr[i][arr.length-1-i];
         }
         System.out.println(res);
         */
        /**
         ArrayList<String> list = new ArrayList<String>();
         list.add("a");
         list.add("b");
         list.add("c");
         String result = "";
         int counter = 0;
         for (String s : list) {
         if (counter == list.size() - 1) {
         result += s;
         counter++;
         } else {
         result += s + ",";
         counter++;
         }
         }
         //System.out.println(result);
         System.out.println(list.toString().replaceAll("[\\[\\]]", ""));
         */
/**
        int arr[][] = createArray(7,7);
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length - i; j++) {
                arr[i][j] = 1;
            }
            for (int j = i; j < arr.length - i; j++) {
                arr[arr.length - 1 - i][j] = 1;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
 */

        fact(5);
    }


    public static int[][] createArray(int width, int height) {
        int arr[][] = new int[width][height];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                arr[i][j] = 0;
            }
        }

        return arr;
    }

    public static int fact(int size) {
        System.out.println("size before: " + size);
        if (size == 1) {
            return 1;
        }

        size = size * fact(size - 1);
        System.out.println("size after: " + size);
        return size;
    }

    public static void one(){
        two();
    }

    public static void two(){
        throw new RuntimeException("from two");
    }
}
