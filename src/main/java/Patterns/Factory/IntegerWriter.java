package Patterns.Factory;

/**
 * Created by Andrey Nazarov on 8/16/2017.
 */
public class IntegerWriter extends AbstractWriter{
    public void write(){
        System.out.println("Integer");
    }
}
