package Patterns.Factory;

/**
 * Created by Andrey Nazarov on 8/16/2017.
 */
public class WriterFactory {
    public AbstractWriter getPrinter(Object object){
        AbstractWriter writer = null;
        if(object instanceof String){
            writer = new StringWriter();
        }else if(object instanceof Integer){
            writer = new IntegerWriter();
        }

        return writer;
    }
}
