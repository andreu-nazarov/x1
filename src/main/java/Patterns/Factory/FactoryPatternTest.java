package Patterns.Factory;

public class FactoryPatternTest {
    public static void main(String[] args) {
        WriterFactory factory = new WriterFactory();
        factory.getPrinter(new Integer(123)).write();
        factory.getPrinter(new String("test")).write();
        }
    }

