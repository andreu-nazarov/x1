package Patterns.Factory;

/**
 * Created by Andrey Nazarov on 8/16/2017.
 */
public abstract class AbstractWriter {
    abstract void write();
}
