package Patterns.Singleton;

public class TestSingleton {
    private static TestSingleton singleton;

    private TestSingleton() {
    }

    public static TestSingleton getSingleton() {
        if (singleton == null) {
            singleton = new TestSingleton();
        }
        return singleton;
    }
}
