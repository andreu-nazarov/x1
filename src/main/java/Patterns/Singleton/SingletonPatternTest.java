package Patterns.Singleton;

/**
 * Created by Andrey Nazarov on 8/16/2017.
 */
public class SingletonPatternTest {
    public static void main(String[] args) {
        System.out.println(TestSingleton.getSingleton().toString());
    }
}
