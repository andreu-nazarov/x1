package Patterns.Iterator;

/**
 * Created by Andrey Nazarov on 11/29/2017.
 */
public class CustomLinkedList {
    private Link first;

    public CustomLinkedList() {
        this.first = null;
    }

    public void add(int data) {
        Link link = new Link(data);
        link.nextLink = first;
        first = link;
    }

    public boolean hasNext(){
        return first != null;
    }

    public void printList() {
        Link currentLink = first;
        System.out.print("List: ");
        while(currentLink != null) {
            currentLink.printLink();
            currentLink = currentLink.nextLink;
        }
        System.out.println("");
    }
}
