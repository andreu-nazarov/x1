package Patterns.Iterator;

/**
 * Created by Andrey Nazarov on 11/29/2017.
 */
public class App {
    public static void main(String[] args) {
        CustomLinkedList a = new CustomLinkedList();
        a.add(1);
        a.add(2);
        a.add(3);

        a.printList();
    }
}
