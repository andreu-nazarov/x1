package Patterns.Iterator;

/**
 * Created by Andrey Nazarov on 11/29/2017.
 */
public class Link {
    public int data;
    public Link nextLink;

    public Link(int data) {
        this.data = data;
    }

    //Print Link data
    public void printLink() {
        System.out.print("{" + data + "}");
    }
}
