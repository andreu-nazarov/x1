package OOP;

/**
 * Created by Andrey Nazarov on 11/22/2017.
 */
public class ChildA extends ParentA {
    public void printClassNameAndParentClassName(){
        super.printClassNameAndParentClassName();
        System.out.println("ChildA");
    }

    public static void st(){
        ParentA.st();
        System.out.println("child-st");
    }
}
