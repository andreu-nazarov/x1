package x1.modules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import x1.pages.*;

/**
 * Created by Andrey on 07.08.2017.
 */
public class MainMenuModule extends Module {
    private Logger logger = LoggerFactory.getLogger(MainMenuModule.class);

    @FindBy(xpath = "//a[contains(text(), 'Home')]") private WebElement home_menulink;

    @FindBy(xpath = "//a[contains(text(), 'What we do')]") private WebElement whatWeDo_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Who we are')]") private WebElement whatWeAre_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Work with us')]") private WebElement workWithUs_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Careers')]") private WebElement careers_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Investor Relations')]") private WebElement investorRelations_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Press')]") private WebElement press_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Magazine')]") private WebElement magazine_menulink;

    @FindBy(xpath = "//ul[@id='menu-main']") private WebElement mainMenu;

    public MainMenuModule(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public KreditechLandingPage clickHomeMenuItem(){
        home_menulink.click();
        logger.info("'Home' menu item clicked");

        return new KreditechLandingPage(driver);
    }

    public WhatWeDoPage clickWhatWeDoMenuItem(){
        whatWeDo_menulink.click();
        logger.info("'What we do' menu item clicked");

        return new WhatWeDoPage(driver);
    }

    public WhatWeArePage clickWhatWeAreMenuItem(){
        whatWeAre_menulink.click();
        logger.info("'Who we are' menu item clicked");

        return new WhatWeArePage(driver);
    }

    public WorkWithUsPage clickWorkWithUsMenuItem(){
        workWithUs_menulink.click();
        logger.info("'Work with us' menu item clicked");

        return new WorkWithUsPage(driver);
    }

    public CareersPage clickCareersMenuItem(){
        careers_menulink.click();
        logger.info("'Careers' menu item clicked");

        return new CareersPage(driver);
    }

    public InvestorRelationsPage clickInvestorRelationsMenuItem(){
        investorRelations_menulink.click();
        logger.info("'Investor Relations' menu item clicked");

        return new InvestorRelationsPage(driver);
    }

    public PressPage clickPressMenuItem(){
        press_menulink.click();
        logger.info("'Press' menu item clicked");

        return new PressPage(driver);
    }

    public MagazinePage clickMagazineMenuItem(){
        magazine_menulink.click();
        logger.info("'Magazine' menu item clicked");

        return new MagazinePage(driver);
    }
}
