package x1.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import x1.modules.MainMenuModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * Created by Andrey on 07.08.2017.
 */
public class KreditechLandingPage extends Page {
    private Logger logger = LoggerFactory.getLogger(KreditechLandingPage.class);
    public static final String PAGE_URL = "https://www.kreditech.com/";

    @FindBy(xpath = "//div[contains(@class, 'linkedin')]/a") private WebElement linkedin_link;

    @FindBy(xpath = "//div[contains(@class, 'facebook')]/a") private WebElement facebook_link;

    @FindBy(xpath = "//div[contains(@class, 'twitter')]/a") private WebElement twitter_link;

    @FindBy(xpath = "//a[contains(text(), 'Home')]") private WebElement home_menulink;

    @FindBy(xpath = "//a[contains(text(), 'What we do')]") private WebElement whatWeDo_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Who we are')]") private WebElement whatWeAre_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Work with us')]") private WebElement workWithUs_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Careers')]") private WebElement careers_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Investor Relations')]") private WebElement investorRelations_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Press')]") private WebElement press_menulink;

    @FindBy(xpath = "//a[contains(text(), 'Magazine')]") private WebElement magazine_menulink;

    public MainMenuModule mainMenu;

    public KreditechLandingPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        mainMenu = new MainMenuModule(driver);
        assertEquals(PAGE_URL, this.getUrl());

    }

    public void open() {
        driver.get(PAGE_URL);
    }

    public KreditechLandingPage clickLinkedinSocialButton() {
        linkedin_link.click();
        logger.info("linkedin link clicked");

        return this;
    }

    public KreditechLandingPage clickFacebookSocialButton() {
        facebook_link.click();
        logger.info("facebook link clicked");

        return this;
    }

    public KreditechLandingPage clickTwitterSocialButton() {
        twitter_link.click();
        logger.info("twitter link clicked");

        return this;
    }

    /**
     * Method switches from main tab, verify that URL contains socialAccountId
     * and socialNetwork, close temporary tab and switches back to main tab
     *
     * @param socialAccountId
     * @param socialNetwork
     * @return
     */
    public boolean isSocialAccountOpened(String socialAccountId,
            String socialNetwork) {
        try {
            logger.info("Social account id: " + socialAccountId,
                    " Social network is: " + socialNetwork);
            String kreditechTab = driver.getWindowHandle();
            new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.numberOfWindowsToBe(2));
            switchTabFrom(kreditechTab);
            logger.info("switched to temporary tab");
            waitForLoad();
            String currentUrl = driver.getCurrentUrl();
            logger.info("current URL is: " + currentUrl);
            assertTrue((currentUrl.contains(socialAccountId) && currentUrl
                    .contains(socialNetwork)));
            driver.close();
            driver.switchTo().window(kreditechTab);
            logger.info("switched back to main tab");

            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

}
