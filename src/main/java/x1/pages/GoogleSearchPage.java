package x1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrey on 07.08.2017.
 */
public class GoogleSearchPage extends Page {
    private Logger logger = LoggerFactory.getLogger(GoogleSearchPage.class);

    public static final String PAGE_URL = "https://www.google.com.ua/";

    @FindBy(id = "lst-ib") private WebElement search_input;

    @FindBy(name = "btnK") private WebElement search_button;

    @FindBy(id = "viewport") private WebElement viewport_div;

    public GoogleSearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        assertEquals(PAGE_URL, this.getUrl());
    }

    public GoogleSearchPage fillSearchFieldWith(String query) {
        search_input.sendKeys(query);
        logger.info("search query entered" + query);
        assertEquals(query, this.getSearchFieldData());

        return this;
    }

    public GoogleSearchPage clickSearchButton() {
        search_button.click();
        logger.info("search_button clicked");

        return this;
    }

    /**
     * Performs search by specified query
     *
     * @param query query to search
     * @return The page
     */
    public GoogleSearchPage searchFor(String query) {
        fillSearchFieldWith(query);
        //click Escape key to close proposed results dropdown
        search_input.sendKeys(Keys.ESCAPE);
        logger.info("Focus is removed from search field");
        clickSearchButton();

        return this;
    }

    public GoogleSearchPage clickSearchResultWithText(String text) {
        driver.findElement(By.xpath("//a[contains(text(), '" + text + "')]"))
                .click();
        logger.info("Search result that contains " + text + " clicked");

        return this;
    }

    public String getSearchFieldData() {
        return search_input.getAttribute("value");
    }

}
