package x1.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

/**
 * Created by Andrey on 07.08.2017.
 */
public abstract class Page {
    public WebDriver driver;

    public static final String LINKEDIN_SOCIAL_ID = "2660395";
    public static final String FACEBOOK_SOCIAL_ID = "kreditech";
    public static final String TWITTER_SOCIAL_ID = "kreditech";

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void switchTabFrom(String oldTab) {
        Set<String> windowHandles = driver.getWindowHandles();
        windowHandles.remove(oldTab);

        driver.switchTo().window(windowHandles.iterator().next());
    }

    public void waitForLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver)
                        .executeScript("return document.readyState")
                        .equals("complete");
            }
        };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
    }

}
