package x1.pages;

import org.openqa.selenium.WebDriver;
import x1.modules.MainMenuModule;

import static org.junit.Assert.assertEquals;

/**
 * Created by Andrey on 07.08.2017.
 */
public class WhatWeDoPage extends Page {
    public static final String PAGE_URL = "https://www.kreditech.com/what-we-do/";
    public MainMenuModule mainMenu;

    public WhatWeDoPage(WebDriver driver) {
        super(driver);
        mainMenu = new MainMenuModule(driver);
        assertEquals(PAGE_URL, this.getUrl());

    }

    public void open() {
        driver.get(PAGE_URL);
    }
}
