package Collections;

import java.util.*;

/**
 * Created by Andrey Nazarov on 11/20/2017.
 */
public class AllCollections {
    public static void main(String[] args) {
        Integer[] array = {1, 3, 2, 3};

        // List
        System.out.println();
        System.out.println("-----------------List---------------");
        System.out.println();
        System.out.println("-----------------ArrayList---------------");
        /**
         * 1) Getting element by index is faster
         */
        ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(array));
        System.out.println(arrayList);

        System.out.println();
        System.out.println("-----------------LinkedList---------------");
        /**
         * LinkedList
         * 1) Adding/removing elements is faster
         */
        LinkedList<Integer> linkedList = new LinkedList<Integer>(Arrays.asList(array));
        System.out.println(linkedList);

        System.out.println();
        System.out.println("-----------------Stack---------------");
        /**
         * Stack
         * 1) last-in-first-out stack realization
         */
        Stack<Integer> stack = new Stack<Integer>();
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        System.out.println(stack);

        // Set
        System.out.println();
        System.out.println("-----------------Set---------------");
        System.out.println();
        System.out.println("-----------------HashSet---------------");
        HashSet<Integer> hashSet = new HashSet<Integer>(Arrays.asList(array));
        System.out.println(hashSet);

        System.out.println();
        System.out.println("-----------------TreeSet---------------");
        /**
         * TreeSet
         * 1) Sorted set
         */
        TreeSet<Integer> treeSet = new TreeSet<Integer>(Arrays.asList(array));
        System.out.println(treeSet);

        System.out.println();
        System.out.println("-----------------LinkedHashSet---------------");
        /**
         * LinkedHashSet
         * 1) Retain input order
         */
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<Integer>(Arrays.asList(array));
        System.out.println(linkedHashSet);

        // Map
        System.out.println();
        System.out.println("-----------------MAP---------------");
        System.out.println();
        System.out.println("-----------------Hashtable---------------");
        /**
         * Hashtable
         * 1) Don't allow null as value
         * 2) Synchronized
         * 3) Bad performance
         */
        Hashtable<Integer, String> hashTable = new Hashtable<Integer, String>();
        hashTable.put(1, "one");
        hashTable.put(3, "three");
        hashTable.put(2, "two");
        System.out.println(hashTable);

        System.out.println();
        System.out.println("-----------------HashMap---------------");
        /**
         * HashMap
         * 1) Allow to use null as value
         * 2) Not synchronized
         * 3) Good performance
         */
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        hashMap.put(1, "one");
        hashMap.put(3, "three");
        hashMap.put(2, "two");
        System.out.println(hashMap);

        System.out.println();
        System.out.println("-----------------LinkedHashMap---------------");
        /**
         * HashMap
         * 1) Retain input order
         */
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();
        linkedHashMap.put(1, "one");
        linkedHashMap.put(3, "three");
        linkedHashMap.put(2, "two");
        System.out.println(linkedHashMap);

        System.out.println();
        System.out.println("-----------------TreeMap---------------");
        /**
         * HashMap
         * 1) By default sorted by keys("natural ordering")
         * 2) Sort order can be changed using Comparator
         */
        TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
        treeMap.put(1, "one");
        treeMap.put(3, "three");
        treeMap.put(2, "two");
        System.out.println(treeMap);
    }
}
