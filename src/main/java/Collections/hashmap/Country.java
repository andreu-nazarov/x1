package Collections.hashmap;

/**
 * Created by Andrey Nazarov on 11/21/2017.
 */
public class Country {

    String name;
    Long population;

    public Country(String name, Long population) {
        super();
        this.name = name;
        this.population = population;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Long getPopulation() {
        return population;
    }
    public void setPopulation(Long population) {
        this.population = population;
    }

    // если длина имени в объекте Country - четное число,
    // то возвращаем 31(любое случайное число), а если нечетное - 95 (любое случайное число).
    // указанный ниже метод - это не самый лучший способ генерации хеш-кода,
    // но мы воспользуемся им для более четкого понимания хеш-карт.

    @Override
    public int hashCode() {
        if(this.name.length()==6)
            return 2;
        else if(this.name.length()==5)
            return 33;
        else
            return 25;
    }


    @Override
    public boolean equals(Object obj) {

        Country other = (Country) obj;
        if (population.equals(other.population))
            return true;
        return false;
    }

}
