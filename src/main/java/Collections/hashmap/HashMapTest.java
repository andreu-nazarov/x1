package Collections.hashmap;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Andrey Nazarov on 11/21/2017.
 */
public class HashMapTest {
    public static void main(String[] args) {

        Country india = new Country("India", 1000L);
        Country japan = new Country("Japan", 10000L);

        Country france = new Country("France", 2000L);
        Country russia = new Country("Russia", 20000L);

        Country ukraine = new Country("Ukraine", 50000L);
        Country ukraine1 = new Country("Ukraine1", 50000L);

        HashMap<Country, String> countryCapitalMap = new HashMap<Country, String>();
        countryCapitalMap.put(india, "Delhi"); // hashCode=33
        countryCapitalMap.put(japan, "Tokyo"); // hashCode=33
        countryCapitalMap.put(france, "Paris"); // hashCode=33
        countryCapitalMap.put(russia, "Moscow"); // hashCode=2
        countryCapitalMap.put(ukraine, "Kiev"); // hashCode=25

        countryCapitalMap.put(ukraine1, "Kiev1"); // hashCode=25
        countryCapitalMap.put(null, "null - value"); // hashCode=25

        countryCapitalMap.get(null);

        Iterator<Country> countryCapitalIter = countryCapitalMap.keySet()
                .iterator();//установите
        //debug-точку на этой строке(23)
        while (countryCapitalIter.hasNext()) {
            System.out.println(countryCapitalMap.get(countryCapitalIter.next()));
        }

    }
}
